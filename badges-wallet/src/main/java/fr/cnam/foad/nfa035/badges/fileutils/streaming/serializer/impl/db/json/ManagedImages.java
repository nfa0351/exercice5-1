package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

/**
 * classe énumération pour le type d'image à traiter
 */
public enum ManagedImages {
    jpeg("image/jpeg"), jpg("image/jpeg"), png("image/png"), gif("image/gif");
    private String mimeType;

    /**
     * constructeur de la classe énumération avec 1 paramètre
     * @param mimeType
     */
    ManagedImages(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * getter de la classe, renvoie description complète du type d'image
     * @return mimeType
     */
    public String getMimeType() {
        return mimeType;
    }
}
